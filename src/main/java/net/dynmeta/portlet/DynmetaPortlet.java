package net.dynmeta.portlet;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.util.PortalUtil;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.liferay.portal.kernel.util.WebKeys;

public class DynmetaPortlet extends GenericPortlet {

    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {

        List<String> pagePlaceholders = new ArrayList<String>(Arrays.asList(WebKeys.PAGE_KEYWORDS, WebKeys.PAGE_DESCRIPTION));

        HttpServletRequest request = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
        HttpSession session = request.getSession();
        StringBundler sb = new StringBundler();

        try {
            for (String placeholder: pagePlaceholders) {
                sb = (StringBundler) renderRequest.getAttribute(placeholder);
                if (sb == null) sb = new StringBundler();
                sb.append(session.getAttribute(placeholder));
                renderRequest.setAttribute(placeholder, sb);
            }
        } catch (Exception e) {
            _log.error("Error setting dynamic keywords", e);
        }
    }

    private static Log _log = LogFactoryUtil.getLog(DynmetaPortlet.class);

}